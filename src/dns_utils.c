#include <dns_utils.h>

char *dns_lookup(char *addr_host, struct sockaddr_in *addr_con)
{
	struct hostent *host_entity;
    char *ip;

    ip = (char *) calloc(1025, sizeof(char));
    if (NULL == ip)
    {
        fprintf(stderr, "Error: calloc failed\n");
        exit(EXIT_FAILURE);
    }

    host_entity = gethostbyname(addr_host);
	if (NULL == host_entity)
	{
        fprintf(stderr, "Error: could not resolve address\n");
		return NULL;
	}

	strcpy(ip, inet_ntoa(*(struct in_addr *) host_entity->h_addr));
	(*addr_con).sin_family = host_entity->h_addrtype;
	(*addr_con).sin_port = htons(PORT);
	(*addr_con).sin_addr.s_addr = *(long*)host_entity->h_addr;

	return ip;
}