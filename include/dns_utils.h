#ifndef DNS_LOOKUP_H
#define DNS_LOOKUP_H

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <arpa/inet.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <netdb.h>

#define PORT 0

char *dns_lookup(char *addr_host, struct sockaddr_in *addr_con);

#endif /* DNS_LOOKUP_H */